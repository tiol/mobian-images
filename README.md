# mobian-images

This is the xslt transform for nginx

Configure nginx images site with (for example):

```
        autoindex on;
        autoindex_format xml;

        location / {
                try_files $uri @autoindex;
        }

        location @autoindex {
                xslt_stylesheet /path/to/autoindex.xslt path='$uri';
        }
```

put .assets folder in images site's root

preview available here : https://test.mobian-project.org/
